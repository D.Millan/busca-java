/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication12;

import java.util.ArrayList;

/**
 *
 * @author D. Millan
 */
public class Pessoa {
    int id = 12;
    String nome = "joao", cpf="123";

    public Pessoa(int id, String nome, String cpf) {
        this.id = id;
        this.nome = nome;
        this.cpf=cpf;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public static ArrayList<Pessoa> getAll()
    {
        ArrayList<Pessoa> lista = new ArrayList<>();
        lista.add(new Pessoa(1,"Pedro","12442"));
        lista.add(new Pessoa(2,"Júlio","14542"));
        lista.add(new Pessoa(3,"Antônia","12762"));
        lista.add(new Pessoa(4,"Júlia","23442"));
        lista.add(new Pessoa(5,"Pietro","12454"));
        lista.add(new Pessoa(6,"Matheus","18742"));
        lista.add(new Pessoa(7,"Ana","14262"));
        lista.add(new Pessoa(8,"Lucas","23408"));
        return lista;
    }

    public boolean containsText(String text) {
        if((nome+"@"+cpf+"@"+id).toLowerCase().contains(text.toLowerCase())) return true;
        return false;
    }
}
