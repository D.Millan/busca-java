/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication12;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author D. Millan
 */
public class FXMLDocumentController implements Initializable {
    Pessoa persona;
    
    private Label label;
    @FXML
    private TableView<Pessoa> pessoa;
    @FXML
    private TableColumn<?, ?> idT;
    @FXML
    private TableColumn<?, ?> nomeT;
    @FXML
    private TableColumn<?, ?> cpfT;
    @FXML
    private TextField busca;
    @FXML
    private TextArea nome;
    @FXML
    private TextArea cpf;
    
    @FXML
    private void atualizar() {
        try {
            pessoa.getItems().clear();
            for (Pessoa p : Pessoa.getAll()) {
                if(p.containsText((busca.getText()!=null)?busca.getText():""))
                pessoa.getItems().add(p);
            }
        } catch (Exception e) {
            System.out.println("aa");
        }
    }

    private void makeColumns() {
        idT.setCellValueFactory(new PropertyValueFactory<>("id"));
        nomeT.setCellValueFactory(new PropertyValueFactory<>("nome"));
        cpfT.setCellValueFactory(new PropertyValueFactory<>("cpf"));
        
    }
    
    @FXML
    private void selecionar(MouseEvent event) {
        persona = pessoa.getSelectionModel().getSelectedItem();
        nome.setText(persona.nome);
        cpf.setText(persona.cpf);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        atualizar();
        makeColumns();
    }    

    
}
